package nl.bluesoft.scanmap.api
import akka.actor.{ActorLogging, Actor}
import nl.bluesoft.scanmap.api.Visitors.RequestReceived
import nl.bluesoft.scanmap.cache.VisitorRepository
import nl.bluesoft.scanmap.domain.ScanMap.Visitor

object Visitors {
  case class RequestReceived(ip:String, requestTimestamp:String)
}

class Visitors(index:VisitorRepository) extends Actor with ActorLogging
{
  override def receive: Receive = {
    case event:RequestReceived ⇒
      index.indexVisitor(Visitor(event.ip, event.requestTimestamp))
  }
}
