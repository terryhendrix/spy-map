package nl.bluesoft.scanmap.api
import java.text.SimpleDateFormat
import java.util.Date
import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import com.typesafe.config.Config
import nl.bluesoft.scanmap.api.Visitors.RequestReceived
import nl.bluesoft.scanmap.api.http.{ExtendableRestApi, ViewRoute}
import nl.bluesoft.scanmap.cache.Cache.GetAnalyzedViolations
import nl.bluesoft.scanmap.cache.{ImplicitOffsetAndLimit, ViolationRepository}
import nl.bluesoft.scanmap.domain.ScanMap.{AnalyzedViolation, Violation}
import nl.bluesoft.scanmap.domain.{JsonProtocol, ScanMap}
import nl.bluesoft.scanmap.util.JsonMarshalling
import scala.concurrent.ExecutionContext
import akka.pattern.ask

class RestApi(index: ViolationRepository, cache: ActorRef, visitors:ActorRef)(implicit override val mat: Materializer, override val system: ActorSystem)
  extends ExtendableRestApi[BasicHttpCredentials] with ViewRoute[BasicHttpCredentials] with SprayJsonSupport with JsonProtocol
  with ImplicitOffsetAndLimit with JsonMarshalling
{
  override val config: Config = system.settings.config.getConfig("scan-map")
  override implicit val ec: ExecutionContext = system.dispatcher

  def sdf = new SimpleDateFormat("HH:mm:ss dd-MMM-yyyy")
  /**
   * The main routing logic. First in chain of responsibility.
   * @return
   */
  override protected def extendedRoute(username: String): Route = {
    pathPrefix("api" / "v1" / "spymap") {
      extractClientIP { client ⇒
        path("violations") {
          get {
            parameter('offset ? 0, 'size ? 1000) { (offset, size) ⇒
              complete {
                visitors ! RequestReceived(client.toOption.map(_.getHostAddress).getOrElse("unknown"), sdf.format(new Date()))
                index.getViolations(offset, size).mapTo[Seq[Violation]]
              }
            }
          }
        } ~
        path("violators") {
          get {
            complete {
              visitors ! RequestReceived(client.toOption.map(_.getHostAddress).getOrElse("unknown"), sdf.format(new Date()))
              (cache ? GetAnalyzedViolations).mapTo[Seq[AnalyzedViolation]]
            }
          }
        }
      } ~
      extractUri { uri ⇒
        complete {
          StatusCodes.NotFound → ScanMap.ErrorMessage(s"The resource $uri does not exist.")
        }
      }
    } ~ viewRoute
  }
}
