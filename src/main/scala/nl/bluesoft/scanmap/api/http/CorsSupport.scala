package nl.bluesoft.scanmap.api.http

import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.{Directive0, Route}

/**
 * Add Cross Origin Resource Sharing on your `ExtendableRestApi`
 */
trait CorsSupport[Creds <: HttpCredentials] {
  this:RestApi with ExtendableRoutes[Creds] ⇒

  private lazy val allowedOrigin = {
    val sAllowedOrigin = config.getString("rest.cors.allowed-origin")
    if(sAllowedOrigin.trim == "*")
      `Access-Control-Allow-Origin`.*
    else
      `Access-Control-Allow-Origin`( HttpOrigin(sAllowedOrigin))
  }

  private lazy val allowHeaders = config.getString("rest.cors.allowed-headers").trim.replaceAll("\\s+", "").split(",")

  /**
   * This directive adds access control headers to normal responses.
   * @return
   */
  private def accessControlHeaders: Directive0 = {
    mapResponseHeaders { headers =>
      allowedOrigin +:
        `Access-Control-Allow-Credentials`(allowHeaders.contains("Authorization")) +:
        `Access-Control-Allow-Headers`(allowHeaders:_*) +:
        headers
    }
  }

  /**
   * This handles preflight OPTIONS requests. TODO: see if can be done with rejection handler,
   * otherwise has to be under addAccessControlHeaders
   * @return
   */
  private def preflightRequestHandler: Route = options {
    complete {
      HttpResponse(200).withHeaders(
        `Access-Control-Allow-Methods`(OPTIONS, POST, PUT, GET, DELETE)
      )
    }
  }

  private def corsSupport(r: Route) = accessControlHeaders {
    preflightRequestHandler ~ r
  }

  wrapRoute(corsSupport)
}