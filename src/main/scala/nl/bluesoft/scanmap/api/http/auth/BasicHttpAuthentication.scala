package nl.bluesoft.scanmap.api.http.auth

import akka.http.scaladsl.model.headers.{HttpChallenge, BasicHttpCredentials}
import akka.http.scaladsl.server.Directives

import scala.concurrent.Future

trait BasicHttpAuthentication extends Authentication[BasicHttpCredentials]
{ this:Directives ⇒

  override final def authenticate(credentials: BasicHttpCredentials): Future[AuthenticationResult[String]] = {
    if(handlers.isEmpty)
      log.warning("Got no handlers to handle authentication with.")

    Future.sequence(handlers.map { handler ⇒
      handler.validate(credentials)
    }).map { results ⇒
      results.find(_.isRight).map { r ⇒
        Right(credentials.username)
      }.getOrElse {
        Left[HttpChallenge,String](authChallenge)
      }
    }
  }
}
