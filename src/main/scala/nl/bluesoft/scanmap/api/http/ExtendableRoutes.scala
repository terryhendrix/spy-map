package nl.bluesoft.scanmap.api.http

import akka.http.scaladsl.model.headers.HttpCredentials
import akka.http.scaladsl.server._
import nl.bluesoft.scanmap.api.http.ExtendableRoutes.{RouteWrapper, InternalExtendedRoute}
import nl.bluesoft.scanmap.api.http.auth.Authentication


object ExtendableRoutes{
  type RouteWrapper = Route ⇒ Route
  private[http] type InternalExtendedRoute = () ⇒ Route
}

/**
 * A trait that gives functionality of combining routes trough the `extendRoute`.
 * Please check `ViewRoute` for how this can be used.
 *
 * Override the `extendedRoute` to add the base route (first in chain or responsibility).
 */
trait ExtendableRoutes[Creds <: HttpCredentials] extends Authentication[Creds]
{ this:Directives ⇒

  type Exhausting = Boolean
  private var registeredRoutes:Seq[(InternalExtendedRoute, Exhausting)] =  Seq.empty
  private var wrappedDirectives:Seq[RouteWrapper] =  Seq.empty
  var hasExhausting = false

  /**
   * Extend a route with a lazy route.
   * @param route       A function to create a `Route`.
   * @param exhausting  The route is exhausting if true, meaning every possible request will be handled by the the route.
   */
  def extendRoute(route:  ⇒ Route, exhausting:Boolean = false) = {
    if(registeredRoutes.exists(_._2) && exhausting)
      throw new Exception("Cannot have 2 exhausting routes")

    registeredRoutes = registeredRoutes :+ (() ⇒ route, exhausting)
  }

  /**
   * Route wrapper. Maybe add prefix such as /api/v1/sales.
   * @param wrapper   The RouteWrapper.
   */
  def wrapRoute(wrapper:RouteWrapper) = {
    wrappedDirectives = wrappedDirectives :+ wrapper
  }

  lazy final val route: Route = {
    log.info(s"Building composed route with ${registeredRoutes.size} extended routes.")
    val nonExhausting = registeredRoutes.filter(!_._2).map(_._1).foldLeft {
      authenticateOrRejectWithChallenge(authenticator _) { username ⇒
        extendedRoute(username)
      }
    } { (a,b) ⇒
      a ~ b()
    }

    val chain = registeredRoutes.find(_._2).map(_._1).map { exhaustingRoute ⇒
      nonExhausting ~ exhaustingRoute()
    }.getOrElse(nonExhausting)

    wrappedDirectives.foldLeft(chain) { case (_route, wrapper) ⇒
      wrapper(_route)
    }
  }

  /**
   * The main routing logic. First in chain of responsibility.
   * @return
   */
  protected def extendedRoute(username:String): Route
}
