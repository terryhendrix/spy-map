package nl.bluesoft.scanmap.api.http.auth

import java.time.LocalDateTime

import akka.http.scaladsl.model.headers.{BasicHttpCredentials, HttpChallenge}
import nl.bluesoft.scanmap.api.http.RestApi
import nl.bluesoft.scanmap.api.http.auth.Authentication.AuthenticationResults

import scala.concurrent.Future

/**
 * Database Authentication
 */
trait SystemUserAuthentication extends BasicHttpAuthentication
{
  this: RestApi =>
  registerAuthenticationHandler {
    new Authentication.Handler[BasicHttpCredentials]
    {
      override def validate(credentials: BasicHttpCredentials): Future[Either[HttpChallenge, AuthenticationResults[BasicHttpCredentials]]] = {
        (for {
          user ← settings.username
          pass ← settings.password
        } yield user == credentials.username && pass == credentials.password match {
          case true ⇒
            Right(AuthenticationResults(Some(credentials), LocalDateTime.now()))
          case false ⇒
            log.info(s"Incorrect login from user ${credentials.username}.")
            Left(authChallenge)
        }) match {
          case Some(res) ⇒
            Future.successful(res)
          case None ⇒
            log.info("Missing username or password.")
            Future.successful(Left(authChallenge))
        }
      }
    }
  }
}
