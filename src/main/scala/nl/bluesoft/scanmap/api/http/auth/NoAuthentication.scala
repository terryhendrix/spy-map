package nl.bluesoft.scanmap.api.http.auth

import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.server.Directives

import scala.concurrent.Future

trait NoAuthentication extends Authentication[BasicHttpCredentials]
{ this:Directives ⇒

  override final def authenticate(credentials: BasicHttpCredentials): Future[AuthenticationResult[String]] = {
    Future.successful(Right("anonymous"))
  }
}
