package nl.bluesoft.scanmap.api.http

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.RouteResult.route2HandlerFlow
import akka.http.scaladsl.server._
import akka.util.Timeout
import com.typesafe.config.Config
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Try

object RestApi {
  object Settings extends ExtendedTypeSafeConfig {
    def emptyStringToNone(s:String) =  if(s.trim.isEmpty) None else Some(s)

    def apply(config:Config):Settings = {
      new Settings(
        config.getString(s"rest.interface"),
        config.getInt(s"rest.port"),
        config.getFiniteDuration(s"rest.timeout"),
        Try(emptyStringToNone(config.getString(s"rest.username"))).getOrElse(None),
        Try(emptyStringToNone(config.getString(s"rest.password"))).getOrElse(None)
      )
    }
  }

  case class Settings(interface:String, port:Int, timeout:FiniteDuration, username:Option[String], password:Option[String])
}

trait RestApiLifeCycle {
  def preStart(): Unit = { }
  def postStop(): Unit = { }
}

trait RestApi extends Directives with RestApiLifeCycle
{
  implicit def mat:akka.stream.Materializer
  def config:Config
  def route:Route
  implicit def system:ActorSystem

  implicit lazy val log:LoggingAdapter = Logging(system, s"HTTP ${settings.port}")

  def defaultExceptionHandler =
    ExceptionHandler {
      case e: Throwable =>
        e.printStackTrace()
        Option(e.getCause).foreach(_.printStackTrace())
        extractUri { uri =>
          extractHost { host ⇒
            log.warning("HTTP 500: {} : {} - {}. Call originated from {}", uri, e.getClass.getName, e.getMessage, host)
            complete(StatusCodes.InternalServerError)
          }
        }
    }

  lazy val settings:RestApi.Settings =  RestApi.Settings(config)
  lazy implicit val timeout = Timeout(settings.timeout)

  def initializeRestApi(implicit system:ActorSystem): Future[ServerBinding] = {
    preStart()
    log.info(s"Loading rest api on ${settings.interface}:${settings.port}")
    Http().bindAndHandle(interface = settings.interface, port = settings.port, handler = route2HandlerFlow {
      handleExceptions(defaultExceptionHandler) {
        extractRequest { req ⇒
          log.info(s"${req.method.value} ${req.uri}")
          route
        }
      }
    })
  }

}
