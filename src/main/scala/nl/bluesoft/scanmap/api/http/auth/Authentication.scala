package nl.bluesoft.scanmap.api.http.auth

import java.time.LocalDateTime

import akka.event.LoggingAdapter
import akka.http.scaladsl.model.headers.{HttpChallenge, HttpCredentials}
import akka.http.scaladsl.server.Directives
import nl.bluesoft.scanmap.api.http.auth.Authentication.Handler

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag

object Authentication {
  case class AuthenticationResults[Creds <: HttpCredentials : ClassTag](credentials: Option[Creds], timestamp:LocalDateTime)

  private[auth] abstract class Handler[Creds <: HttpCredentials : ClassTag] {
    def classOfCredentials = implicitly[ClassTag[Creds]].runtimeClass
    def validate(credentials: Creds): Future[Either[HttpChallenge, AuthenticationResults[Creds]]]
  }

  val anonymousUser = "anonymous"
}

trait Authentication[Creds <: HttpCredentials]
{ this:Directives ⇒

  implicit def ec:ExecutionContext

  implicit def log:LoggingAdapter

  private var _authenticationHandlers = Seq.empty[Authentication.Handler[Creds]]

  protected[auth] final def registerAuthenticationHandler(handler:Authentication.Handler[Creds]) = {
    _authenticationHandlers = _authenticationHandlers :+ handler
  }

  /**
   * Decides only once if we should handle authentication.
   * If there are no `Handler` registered, then authentication will not happen.
   */
  lazy val shouldHandle = {
    if(_authenticationHandlers.isEmpty)
      log.warning(s"No authentication handlers are configured, allowing access to all users from all origins.")
    else
      log.info("Will perform HTTP authentication")
    _authenticationHandlers.nonEmpty
  }

  /**
   * Make sure the handlers cannot be modified in after boot.
   */
  lazy val handlers: Seq[Handler[Creds]] = {
    log.info(s"Initializing ${_authenticationHandlers.size} authentication handlers.")
    _authenticationHandlers
  }

  def authChallenge = HttpChallenge(scheme = "Basic", realm = "Novamedia Fundraising API")

  def authenticator(credentials: Option[HttpCredentials]): Future[AuthenticationResult[String]] = {
    if(shouldHandle) {
      credentials match {
        case Some(creds) ⇒
          creds match {
            case cr:Creds ⇒
              authenticate(cr)
            case _ ⇒
              log.error(s"$creds have the incorrect type")
              Future.successful(Left(authChallenge))
          }
        case None ⇒
          Future.successful(Left(authChallenge))
      }
    } else {
      Future.successful(Right(Authentication.anonymousUser))
    }
  }

  def authenticate(credentials:Creds): Future[AuthenticationResult[String]] = {
    Future.successful(Right(Authentication.anonymousUser))
  }
}
