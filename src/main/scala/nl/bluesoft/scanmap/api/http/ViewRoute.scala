package nl.bluesoft.scanmap.api.http

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.HttpCredentials
import com.typesafe.config.Config
import nl.bluesoft.scanmap.util.ops.StringOps
import scala.io.Source

object ViewRoute {
  private[http] object Settings extends ExtendedTypeSafeConfig {
    def apply(config:Config):Settings = {
      new Settings(
        config.getString("rest.view-route.path-to-resources"),
        config.getString("rest.view-route.web-dir"),
        config.getBoolean(s"rest.view-route.developer-mode")
      )
    }
  }

  private[http] case class Settings(pathToResources:String, webDir:String, developerMode:Boolean)
}

trait ViewRoute[Creds <: HttpCredentials] extends StringOps
{ this:RestApi with ExtendableRoutes[Creds] ⇒
  def system:ActorSystem
  protected lazy val viewRouteSettings = ViewRoute.Settings(config)

  lazy val viewRoute = {
    get {
      pathSingleSlash {
        log.info("Permanent Redirect to index.html")
        redirect("index.html", StatusCodes.PermanentRedirect)
      } ~ getResourceFromDir(viewRouteSettings.webDir)
    }
  }

  def getResourceFromDir(path: String) = {
    if (viewRouteSettings.developerMode) {
      log.info(s"[DeveloperMode] $path -> file://${viewRouteSettings.pathToResources.addTrailingSlash + path}")
      getFromDirectory(viewRouteSettings.pathToResources.addTrailingSlash + path)
    }
    else {
      getFromResourceDirectory(path, this.getClass.getClassLoader)
    }
  }

  def getResource(file:String) = {
    if(viewRouteSettings.developerMode) {
      log.info(s"[DeveloperMode] GET $file -> file://${viewRouteSettings.pathToResources.addTrailingSlash + file}")
      getFromDirectory(viewRouteSettings.pathToResources.addTrailingSlash + file)
    }
    else {
      getFromResource(file)
    }
  }

  def getResourceAsString(file:String) = {
    if(viewRouteSettings.developerMode) {
      log.info(s"[DeveloperMode] $file -> file://${viewRouteSettings.pathToResources.addTrailingSlash + file}")
      Source.fromFile(viewRouteSettings.pathToResources.addTrailingSlash + file).mkString
    }
    else {
      Source.fromURL(getClass.getResource(file.addLeadingSlash)).mkString
    }
  }
}

