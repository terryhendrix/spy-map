package nl.bluesoft.scanmap.api.http

import java.util.concurrent.TimeUnit

import com.typesafe.config.Config

import scala.concurrent.duration.{Duration, FiniteDuration}

trait ExtendedTypeSafeConfig
{
  implicit class ExtendedConfig(config:Config) {
    def getFiniteDuration(s:String): FiniteDuration = FiniteDuration(Duration(config.getString(s)).toMillis, TimeUnit.MILLISECONDS)
  }
}
