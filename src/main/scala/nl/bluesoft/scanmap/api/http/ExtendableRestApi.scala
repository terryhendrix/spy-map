package nl.bluesoft.scanmap.api.http

import akka.http.scaladsl.model.headers.HttpCredentials

/**
 * A `RestApi` with `ExtendableRoutes`. Override `extendedRoute` to add your custom rest api calls.
 */
trait ExtendableRestApi[Creds <: HttpCredentials] extends RestApi with ExtendableRoutes[Creds]