package nl.bluesoft.scanmap
import akka.actor.{ActorSystem, Props}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import nl.bluesoft.scanmap.api.{RestApi, Visitors}
import nl.bluesoft.scanmap.cache.{Cache, ViolationIndex, VisitorIndex}
import nl.bluesoft.scanmap.system.ScanMapAnalyzer

import scala.concurrent.duration._
import scala.language.postfixOps

object Boot extends App
{
  implicit val system = ActorSystem("ScanMap")

  implicit val log = system.log

  val decider: Supervision.Decider = {
    case ex: spray.json.DeserializationException ⇒
      log.error(s"${ex.getMessage}")
      Supervision.Resume
    case ex                           =>
      ex.printStackTrace()
      Supervision.Resume
  }

  implicit val mat = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))

  implicit val ec = system.dispatcher

  val violationIndex = new ViolationIndex()
  violationIndex.createIndex()

  val analyzer = new ScanMapAnalyzer(violationIndex)

  val visitorIndex = new VisitorIndex()
  visitorIndex.createIndex()

  val visitors = system.actorOf(Props(new Visitors(visitorIndex)))
  
  val cache = system.actorOf(Props(new Cache), "Cache")

  val api = new RestApi(violationIndex, cache, visitors)

  val binding = api.initializeRestApi

  val analyzing = system.scheduler.schedule(5 seconds, 15 minutes, new Runnable() {
    override def run(): Unit = analyzer.indexAndAnalyze
  })
}
