package nl.bluesoft.scanmap.system

import akka.NotUsed
import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import nl.bluesoft.scanmap.cache.ViolationRepository
import nl.bluesoft.scanmap.domain.JsonProtocol
import nl.bluesoft.scanmap.domain.ScanMap.{IpGeoDecodeResponse, FirewallViolation}
import nl.bluesoft.scanmap.util.JsonMarshalling
import nl.bluesoft.scanmap.util.ops.{StringOps, AkkaHttpOps}
import scala.concurrent.{ExecutionContext, Future}

object IpInfo {
}

trait IpInfo extends StringOps with AkkaHttpOps with JsonMarshalling with JsonProtocol
{
  def system:ActorSystem
  def log:LoggingAdapter
  implicit def mat:ActorMaterializer
  implicit def ec:ExecutionContext

  def index:ViolationRepository
  def baseUri:String
  def ipInfoParallelism:Int

  def geoDecode(ipAddress:String):Future[IpGeoDecodeResponse] = {
    log.info(s"Resolving location for ip $ipAddress at $baseUri")
    Http(system).singleRequest(HttpRequest(
      method = HttpMethods.GET,
      uri = baseUri + ipAddress + "/geo"
    )).flatMap(_.entity.bodyAsString).map { json ⇒
      json.asObject[IpGeoDecodeResponse]
    }
  }

  def geoDecode: Flow[FirewallViolation, (FirewallViolation, IpGeoDecodeResponse), NotUsed] = {
    Flow[FirewallViolation].mapAsync(ipInfoParallelism) { violation ⇒
      index.violationsByIP(violation.ipAddress).flatMap { byIp ⇒
        byIp.find(v ⇒ !v.latitude.isEmpty && !v.latitude.isEmpty) match {
          case Some(alreadyResolved) ⇒
            log.info(s"Using cache for location of ${alreadyResolved.ip}")
            val mockedResponse = IpGeoDecodeResponse(
              ip = alreadyResolved.ip,
              city = alreadyResolved.city,
              region = alreadyResolved.region,
              loc = alreadyResolved.latitude+","+alreadyResolved.longitude,
              country = alreadyResolved.countryCode,
              postal = alreadyResolved.postal
            )
            Future.successful((violation, mockedResponse))

          case None ⇒
            geoDecode(violation.ipAddress).map((violation,_))
        }
      }
    }
  }

}
