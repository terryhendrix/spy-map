package nl.bluesoft.scanmap.system

import akka.actor.ActorLogging
import akka.stream.actor.ActorPublisher
import akka.stream.actor.ActorPublisherMessage.{Cancel, Request}
import nl.bluesoft.scanmap.system.BatchPublisher.{Size, Offset}
import scala.annotation.tailrec
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

object BatchPublisher {
  type Offset = Int
  type Size = Int
}

class BatchPublisher[U](batchSize:Size)(retrieveBatch:(Offset, Size) ⇒ Future[Seq[U]]) extends ActorPublisher[U] with ActorLogging
{
  implicit val ec = context.dispatcher
  var currentOffset:Offset = 0
  var buffer = scala.collection.mutable.Seq.empty[U]

  override def receive: Receive = {
    case Request(cnt) ⇒
      sendData(cnt)

    case Cancel =>
      context.stop(self)

    case any =>
      log.warning(s"Unhandled $any")
  }

  @tailrec final def sendData(cnt:Long):Unit = {
    if(cnt >= buffer.size || buffer.size < batchSize / 2) {
      val res = Await.result(retrieveBatch(currentOffset, batchSize), 5 seconds)
      currentOffset = currentOffset + res.size
      buffer = buffer ++ res
      log.debug(s"Adding ${res.size} items to the buffer. Size now is ${buffer.size}.")
    }

    if(buffer.isEmpty && !isCompleted) {
      log.debug("Done publishing.")
      onComplete()
    }
    else if(cnt > 0 && !isCompleted) {
      val p = buffer.head
      val l = buffer.tail
      log.debug(s"Publishing item from buffer. Buffer size from ${buffer.size} to ${l.size}.")
      onNext(p)
      buffer = l
      sendData(cnt - 1)
    }
  }

  override def preStart() = log.debug("Starting")

  override def postStop() = log.debug("Terminated")
}