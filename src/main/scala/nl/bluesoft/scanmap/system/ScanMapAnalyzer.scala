package nl.bluesoft.scanmap.system
import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import nl.bluesoft.scanmap.cache.ViolationRepository
import nl.bluesoft.scanmap.domain.Events.ViolationsAnalyzed
import nl.bluesoft.scanmap.domain.KnownPortNumbers
import nl.bluesoft.scanmap.domain.ScanMap.{AnalyzedViolation, ViolationSummary}

import scala.concurrent.{ExecutionContext, Future}

class ScanMapAnalyzer(override val index:ViolationRepository)(implicit override val system:ActorSystem, override val mat:ActorMaterializer)
  extends LogFileReader with IpInfo with Indexer
{
  override implicit val ec: ExecutionContext = system.dispatcher
  override val baseUri = system.settings.config.getString("scan-map.ip-info.base-uri").addTrailingSlash
  override val logFile: String = system.settings.config.getString("scan-map.log-file")
  override val ipInfoParallelism:Int = system.settings.config.getInt("scan-map.ip-info.parallel-requests")
  override val parallelIndexActions:Int = system.settings.config.getInt("scan-map.parallel-index-actions")

  override val log: LoggingAdapter = Logging(system, "Analyzer")

  def indexAndAnalyze: Future[Seq[AnalyzedViolation]] = {
    log.info("Indexing...")
    readUfwViolations via geoDecode via indexer runWith Sink.ignore flatMap { _ ⇒
      log.info("Done indexing.")
      analyse
    }
  }

  def analyse: Future[Seq[AnalyzedViolation]] = {
    log.info("Analyzing....")
    index.violations.runFold(Seq.empty[AnalyzedViolation]) { case (groups, v) ⇒
      val summary = ViolationSummary(
        port = v.port,
        description = KnownPortNumbers.find(v.port.toInt, v.protocol).map(_.description).getOrElse("Unknown"),
        protocol = v.protocol,
        dateDetected = v.dateDetected)
      val index = groups.indexWhere(_.ip == v.ip)
      index match {
        case -1 ⇒
          groups :+ AnalyzedViolation(
            ip = v.ip,
            city = v.city,
            region = v.region,
            country = v.country,
            countryCode = v.countryCode,
            latitude = v.latitude,
            longitude = v.longitude,
            postal = v.postal,
            violations = Seq(summary)
          )
        case found ⇒
          val group = groups(found)
          groups.updated(found, group.copy(violations = group.violations :+ summary))
      }
    }.map { groups ⇒
      log.info(s"Done analyzing, found ${groups.size} IP's")
      system.eventStream.publish(ViolationsAnalyzed(groups))
      groups
    }
  }
}
