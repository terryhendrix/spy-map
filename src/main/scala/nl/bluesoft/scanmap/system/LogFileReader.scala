package nl.bluesoft.scanmap.system
import java.nio.file.Paths
import akka.event.LoggingAdapter
import akka.stream.{OverflowStrategy, IOResult}
import akka.stream.scaladsl.{Framing, Source, FileIO}
import akka.util.ByteString
import nl.bluesoft.scanmap.domain.ScanMap.FirewallViolation
import scala.concurrent.Future

object LogFileReader {
}

trait LogFileReader
{
  def log:LoggingAdapter
  def logFile:String

  def getVariable(line:String, key:String) = {
    line.split(key+"=")(1).split(" ")(0)
  }

  def readUfwViolations: Source[FirewallViolation, Future[IOResult]] = {
    FileIO.fromPath(Paths.get(logFile))
      .via(Framing.delimiter(ByteString("\n"), 8192, allowTruncation = true))
      .map(_.decodeString("UTF-8"))
      .filter(_.toUpperCase.contains("UFW BLOCK"))
      .map { line ⇒
        val violation = FirewallViolation(
          ipAddress = getVariable(line, "SRC"),
          port = getVariable(line, "DPT"),
          protocol = getVariable(line, "PROTO"),
          dateDetected = line.split(" ").take(3).mkString(" ")
        )
        violation
      }
      .buffer(100, OverflowStrategy.backpressure)
  }
}
