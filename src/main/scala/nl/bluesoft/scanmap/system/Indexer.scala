package nl.bluesoft.scanmap.system
import akka.event.LoggingAdapter
import akka.stream.scaladsl.Flow
import nl.bluesoft.scanmap.cache.ViolationRepository
import nl.bluesoft.scanmap.domain.{KnownPortNumbers, CountryCodes}
import nl.bluesoft.scanmap.domain.ScanMap.{FirewallViolation, IpGeoDecodeResponse, Violation}

trait Indexer
{
  def index: ViolationRepository
  def log:LoggingAdapter
  def parallelIndexActions: Int

  def indexer = {
    Flow[(FirewallViolation, IpGeoDecodeResponse)].mapAsync(parallelIndexActions) { case (v, geo) ⇒
      val com = Violation(
        ip = v.ipAddress,
        port = v.port.toInt,
        protocol = v.protocol,
        city = geo.city,
        region = geo.region,
        country = CountryCodes.translate(geo.country),
        countryCode = geo.country,
        latitude = geo.loc.split(",")(0),
        longitude = geo.loc.split(",")(1),
        postal = geo.postal,
        dateDetected = v.dateDetected,
        description = KnownPortNumbers.find(v.port.toInt, v.protocol).map(_.description).getOrElse("Unknown"))
      index.indexViolation(com)
    }
  }
}
