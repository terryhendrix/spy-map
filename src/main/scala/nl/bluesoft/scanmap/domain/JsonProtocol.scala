package nl.bluesoft.scanmap.domain
import nl.bluesoft.scanmap.domain.ScanMap._
import spray.json.DefaultJsonProtocol

trait JsonProtocol extends DefaultJsonProtocol
{
  implicit val errorMessageFormat = jsonFormat1(ScanMap.ErrorMessage)
  implicit val firewallViolationFormat = jsonFormat4(FirewallViolation)
  implicit val ipGeoDecodeResponseFormat = jsonFormat6(IpGeoDecodeResponse)
  implicit val violationFormat = jsonFormat12(Violation)
  implicit val violationSummaryFormat = jsonFormat4(ViolationSummary)
  implicit val analyzedViolationFormat = jsonFormat9(AnalyzedViolation)
  implicit val visitorFormat = jsonFormat2(Visitor)
}

object JsonProtocol extends JsonProtocol