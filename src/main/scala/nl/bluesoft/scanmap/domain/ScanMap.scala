package nl.bluesoft.scanmap.domain

object ScanMap
{
  case class ErrorMessage(description:String)

  case class FirewallViolation(ipAddress:String, port:String, protocol:String, dateDetected:String)

  case class IpGeoDecodeResponse(ip:String, city:String, region:String, country:String, loc:String, postal:Option[String])

  case class Violation(ip:String, port:Int, protocol:String, description:String, city:String, region:String, country:String, countryCode:String,
                       latitude:String, longitude:String, postal:Option[String], dateDetected:String)

  case class ViolationSummary(port:Int, description:String, protocol:String, dateDetected:String)

  case class AnalyzedViolation(ip:String, city:String, region:String, country:String, countryCode:String,
                              latitude:String, longitude:String, postal:Option[String], violations:Seq[ViolationSummary])

  case class Visitor(ip:String, lastVisit:String)
}
