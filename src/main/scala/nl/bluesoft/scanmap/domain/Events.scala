package nl.bluesoft.scanmap.domain
import nl.bluesoft.scanmap.domain.ScanMap.AnalyzedViolation

object Events
{
  case class ViolationsAnalyzed(violations:Seq[AnalyzedViolation])
}
