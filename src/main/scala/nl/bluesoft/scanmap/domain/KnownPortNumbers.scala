package nl.bluesoft.scanmap.domain

object KnownPortNumbers
{
  case class KnownPort(port: Int, protocol: String, description: String)
  private val known = Seq(
    KnownPort(21, "TCP", "File Transfer Protocol"),
    KnownPort(22, "TCP", "SSH"),
    KnownPort(23, "TCP", "Telnet"),
    KnownPort(25, "TCP", "SMTP"),
    KnownPort(43, "TCP", "WHOIS"),
    KnownPort(53, "TCP", "DNS"),
    KnownPort(67, "TCP", "DHCP Server"),
    KnownPort(68, "TCP", "DHCP Client"),
    KnownPort(70, "TCP", "Gopher Protocol"),
    KnownPort(79, "TCP", "Finger protocol"),
    KnownPort(110, "TCP", "POP3, (for receiving email)"),
    KnownPort(119, "TCP", "NNTP (Network News Transfer Protocol)"),
    KnownPort(143, "TCP", "IMAP4, Protocol (for email service)"),
    KnownPort(194, "TCP", "IRC"),
    KnownPort(389, "TCP", "LDAP (light weight directory access)"),
    KnownPort(443, "TCP", "Secure HTTP over SSL (https)"),
    KnownPort(465, "TCP", "Secure SMTP (email) using SSL"),
    KnownPort(990, "TCP", "Secure FTP using SSL"),
    KnownPort(993, "TCP", "Secure IMAP protocol over SSL (for emails)"),
    KnownPort(1433, "TCP", "Microsoft SQL server port"),
    KnownPort(2082, "TCP", "Cpanel default port"),
    KnownPort(2083, "TCP", "Cpanel over SSL"),
    KnownPort(2086, "TCP", "Cpanel Webhost Manager (default)"),
    KnownPort(2087, "TCP", "Cpanel Webhost Manager (with https)"),
    KnownPort(2095, "TCP", "Cpanel Webmail"),
    KnownPort(2096, "TCP", "Cpanel secure webmail over SSL"),
    KnownPort(2222, "TCP", "DirectAdmin Server Control Panel"),
    KnownPort(3306, "TCP", "MySQL Database Server"),
    KnownPort(4643, "TCP", "Virtuosso Power Panel"),
    KnownPort(5432, "TCP", "PostgreSQL Database Server"),
    KnownPort(8080, "TCP", "HTTP port (alternative one for port 80,)"),
    KnownPort(8087, "TCP", "Plesk Control Panel Port (default)"),
    KnownPort(8443, "TCP", "Plesk Server Control Panel over SSL"),
    KnownPort(9999, "TCP", "Urchin Web Analytics"),
    KnownPort(10000, "TCP", "Webmin Server Control Panel"),
    KnownPort(19638, "TCP", "Ensim Server Control Panel")
  )

  def find(port: Int, protocol: String): Option[KnownPort] = {
    val pr = protocol.toUpperCase
    known.find(k ⇒ k.port == port && k.protocol == pr)
  }

}