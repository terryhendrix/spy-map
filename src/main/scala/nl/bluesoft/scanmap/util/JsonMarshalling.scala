package nl.bluesoft.scanmap.util

import spray.json.{JsonReader, JsonWriter, _}

import scala.util.Try

trait JsonMarshalling extends AnyRef
{
  implicit class As(json:String) {
    def asObject[U:JsonReader] = json.parseJson.convertTo[U]
    def isJson:Boolean = Try(json.parseJson).isSuccess
  }

  implicit class AsJson[U](u:U) {
    def asJson(implicit writer: JsonWriter[U]) = u.toJson.prettyPrint
    def asCompactJson(implicit writer: JsonWriter[U]) = u.toJson.compactPrint
  }
}

object JsonMarshalling extends JsonMarshalling