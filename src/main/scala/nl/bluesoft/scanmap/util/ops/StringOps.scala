package nl.bluesoft.scanmap.util.ops

import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.TimeUnit

import akka.util.Timeout

import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.Try

trait StringOps
{
  private val classpath = """classpath://(.*)""".r
  private val file = """file://(.*)""".r
  private val http = """http://(.*)""".r

  implicit class StringOps(s:String) {
    def fileUrl:Option[String] = {
      s match {
        case file(path) ⇒ Some(path)
        case _ ⇒ None
      }
    }

    def httpUrl:Option[String] = {
      s match {
        case http(path) ⇒ Some(path)
        case _ ⇒ None
      }
    }

    def classpathUrl:Option[String] = {
      s match {
        case classpath(path) ⇒ Some(path)
        case _ ⇒ None
      }
    }

    def addTrailingSlash = {
      if(s.endsWith("/")) s else s + "/"
    }

    def addLeadingSlash = {
      if(s.startsWith("/")) s else "/" + s
    }

    def upperCaseFirst:String = {
      Character.toUpperCase(s.charAt(0)) + s.substring(1)
    }

    def toLowerCaseFirst = {
      Character.toLowerCase(s.charAt(0)) + s.substring(1)
    }

    def toOptionString: Option[String] = {
      if(s.trim.isEmpty) None
      else Option(s)
    }

    def toDuration: Duration = {
      Duration(s)
    }

    def toFiniteDuration: FiniteDuration = {
      FiniteDuration(Duration(s).toMillis, TimeUnit.MILLISECONDS)
    }

    def toTimeout: Timeout = {
      Timeout(s.toFiniteDuration)
    }

    def toDate: Option[Date] = Try {
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss.SSSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss.SSS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss.SSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss.SS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ssX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-ddHH:mm:ssX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SS").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd HH:mm:ssX").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy/MM/dd").parse(s)
    }.recover { case t: Throwable =>
      new SimpleDateFormat("yyyy-MM-dd").parse(s)
    }.toOption
  }
}

object StringOps extends StringOps