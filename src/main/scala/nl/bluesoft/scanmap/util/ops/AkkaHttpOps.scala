package nl.bluesoft.scanmap.util.ops

import akka.http.scaladsl.model.HttpEntity
import akka.stream.Materializer
import scala.concurrent.{ExecutionContext, Future}

trait AkkaHttpOps
{
  implicit class AkkaHttpOps(entity:HttpEntity) {
    def bodyAsString(implicit mat:Materializer, ec:ExecutionContext): Future[String] = {
      entity.dataBytes.runFold(new StringBuilder) {
        case (builder, bytes) ⇒
          builder.append(bytes.decodeString("UTF-8"))
      }.map(_.toString())
    }
  }
}

object AkkaHttpOps
