package nl.bluesoft.scanmap.cache
import akka.actor.{ActorRef, ActorSystem}
import akka.event.LoggingAdapter
import akka.stream.scaladsl.Source
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.mappings.FieldType._
import com.sksamuel.elastic4s.mappings.TypedFieldDefinition
import nl.bluesoft.scanmap.cache.ESDataTypes.{SearchOffset, SearchSize}
import nl.bluesoft.scanmap.domain.ScanMap.Violation
import org.elasticsearch.search.sort.SortOrder

import scala.concurrent.{ExecutionContext, Future}

trait ViolationRepository extends ImplicitOffsetAndLimit {
  def indexViolation(v:Violation):Future[Boolean]
  def violationsByIP(ip:String)(implicit offset:SearchOffset = 0, limit:SearchSize = 100):Future[Seq[Violation]]
  def getViolations(implicit offset:SearchOffset, limit:SearchSize): Future[Seq[Violation]]
  def violations: Source[Violation, ActorRef]
}

class ViolationIndex(implicit override val system:ActorSystem, implicit override val log:LoggingAdapter)
  extends ViolationRepository with ElasticSearchRepoLike
{
  override val indexName: IndexName = "violations"

  override implicit val ec: ExecutionContext = system.dispatcher

  override def indexDefinition: Seq[TypedFieldDefinition] =  Seq(
    "city" typed StringType index notAnalyzed,
    "country" typed StringType index notAnalyzed,
    "countryCode" typed StringType index notAnalyzed,
    "ip" typed StringType index notAnalyzed,
    "latitude" typed StringType index notAnalyzed,
    "description" typed StringType index notAnalyzed,
    "longitude" typed StringType index notAnalyzed,
    "port" typed IntegerType,
    "postal" typed StringType index notAnalyzed,
    "protocol" typed StringType index notAnalyzed,
    "region" typed StringType index notAnalyzed,
    "dateDetected" typed StringType index notAnalyzed
  )

  def indexViolation(v:Violation) = {
    doIndex(id = v.ip + "_" + v.dateDetected.replaceAll(" ", "_"))(
      "city" -> v.city,
      "country" -> v.country,
      "countryCode" -> v.countryCode,
      "ip" -> v.ip,
      "latitude" -> v.latitude,
      "longitude" -> v.longitude,
      "description" -> v.description,
      "port" -> v.port,
      "postal" -> v.postal.orNull,
      "protocol" -> v.protocol,
      "region" -> v.region,
      "dateDetected" → v.dateDetected
    ).mapCreated
  }

  def violationsByIP(ip:String)(implicit offset:SearchOffset, limit:SearchSize):Future[Seq[Violation]] = {
    query(
      bool(must(
        termQuery("ip", ip)
      ))
    ).mapResults[Violation]
  }

  def getViolations(implicit offset:SearchOffset, limit:SearchSize): Future[Seq[Violation]] = {
    list(sortBy = "dateDetected", order = SortOrder.DESC).mapResults[Violation]
  }

  override def violations: Source[Violation, ActorRef] = {
    stream[Violation](sortBy = "dateDetected", order = SortOrder.DESC, batchSize = 1000)
  }
}
