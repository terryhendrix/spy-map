package nl.bluesoft.scanmap.cache
import akka.actor.{Actor, ActorLogging}
import nl.bluesoft.scanmap.cache.Cache.GetAnalyzedViolations
import nl.bluesoft.scanmap.domain.Events.ViolationsAnalyzed
import nl.bluesoft.scanmap.domain.ScanMap.AnalyzedViolation

object Cache {
  case object GetAnalyzedViolations
}

class Cache extends Actor with ActorLogging
{
  var state = Seq.empty[AnalyzedViolation]

  override def preStart() = {
    context.system.eventStream.subscribe(self, classOf[ViolationsAnalyzed])
  }

  override def postStop() = {
    context.system.eventStream.unsubscribe(self)
  }

  override def receive: Receive = {
    case event:ViolationsAnalyzed ⇒
      log.info(s"Updated cache with ${event.violations.size} violators.")
      state = event.violations

    case GetAnalyzedViolations ⇒
      sender ! state
  }
}
