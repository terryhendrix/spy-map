package nl.bluesoft.scanmap.cache
import akka.actor.{ActorRef, ActorSystem}
import akka.event.LoggingAdapter
import akka.stream.scaladsl.Source
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.mappings.FieldType._
import com.sksamuel.elastic4s.mappings.TypedFieldDefinition
import nl.bluesoft.scanmap.cache.ESDataTypes.{SearchOffset, SearchSize}
import nl.bluesoft.scanmap.domain.ScanMap.Visitor
import org.elasticsearch.search.sort.SortOrder

import scala.concurrent.{ExecutionContext, Future}

trait VisitorRepository {
  def indexVisitor(v:Visitor):Future[Boolean]
  def getVisitors(implicit offset:SearchOffset, limit:SearchSize): Future[Seq[Visitor]]
  def visitors: Source[Visitor, ActorRef]
}

class VisitorIndex(implicit override val system:ActorSystem, implicit override val log:LoggingAdapter)
  extends VisitorRepository with ElasticSearchRepoLike
{
  override val indexName: IndexName = "visitors"

  override implicit val ec: ExecutionContext = system.dispatcher

  override def indexDefinition: Seq[TypedFieldDefinition] =  Seq(
    "ip" typed StringType index notAnalyzed,
    "lastVisit" typed StringType index notAnalyzed
  )

  def indexVisitor(v:Visitor) = {
    doIndex(id = v.ip)(
      "ip" -> v.ip,
      "lastVisit" -> v.lastVisit
    ).mapCreated
  }

  def getVisitors(implicit offset:SearchOffset, limit:SearchSize): Future[Seq[Visitor]] = {
    list(sortBy = "lastVisit", order = SortOrder.DESC).mapResults[Visitor]
  }

  override def visitors: Source[Visitor, ActorRef] = {
    stream[Visitor](sortBy = "lastVisit", order = SortOrder.DESC, batchSize = 1000)
  }
}
