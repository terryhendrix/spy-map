package nl.bluesoft.scanmap.cache

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.event.LoggingAdapter
import akka.stream.scaladsl.Source
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.mappings.TypedFieldDefinition
import com.sksamuel.elastic4s.{AbstractAggregationDefinition, QueryDefinition}
import nl.bluesoft.scanmap.cache.ESDataTypes.{DocumentSize, ResultSize, SearchOffset, SearchSize}
import nl.bluesoft.scanmap.domain.JsonProtocol
import nl.bluesoft.scanmap.system.BatchPublisher
import nl.bluesoft.scanmap.util.JsonMarshalling
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.search.aggregations.Aggregation
import org.elasticsearch.search.sort.SortOrder
import spray.json._

import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions
import scala.util.Try

trait ElasticSearchTypes extends ImplicitOffsetAndLimit {
  type Index = (String)
  type Analyzer = (String)
  type IndexName = (String)
}

object ESDataTypes{
  case class SearchOffset(offset:Int)
  case class SearchSize(limit:Int)
  case class ResultSize(size:Int)
  case class DocumentSize(size:Int)
}

trait ImplicitOffsetAndLimit {
  implicit def intToSearchOffset(c:Int): SearchOffset = ESDataTypes.SearchOffset(c)
  implicit def intToSearchLimit(c:Int): SearchSize = ESDataTypes.SearchSize(c)
  implicit def intToDocumentSize(c:Int): DocumentSize = ESDataTypes.DocumentSize(c)
  implicit def intToResultSize(c:Int): ResultSize = ESDataTypes.ResultSize(c)
}

trait ElasticSearchRepoLike extends ElasticSearchUtils with JsonProtocol with ElasticSearchIndexDefinitionLike
{
  def system: ActorSystem
  implicit def ec:ExecutionContext
  implicit def log:LoggingAdapter
  val es = ElasticSearch(system)

  val notAnalyzed: Analyzer = "not_analyzed"
  val caseInsensitive: Analyzer = "case_insensitive"
  val analyzed: Analyzer = "analyzed"

  def indexName:IndexName

  def doIndex(id:String)(indexDef:(String,Any)*) = {
    es.doIndex(index.into(indexName -> "view").fields(indexDef).id(id)).logException
  }

  def query(implicit offset:SearchOffset, limit:SearchSize):Future[SearchResponse] = {
    es.doSearch(search.in(indexName -> "view").from(offset.offset).size(limit.limit)).logException
  }

  def query(queryDef:QueryDefinition)
           (implicit offset:SearchOffset, limit:SearchSize):Future[SearchResponse] = {
    es.doSearch(search.in(indexName -> "view").from(offset.offset).size(limit.limit).query(queryDef)).logException
  }

  def query(sortBy:String, order:SortOrder)
           (queryDef:QueryDefinition)
           (implicit offset:SearchOffset, limit:SearchSize):Future[SearchResponse] = {
    es.doSearch(search.in(indexName -> "view").from(offset.offset).size(limit.limit).query(queryDef).sort(by field sortBy order order)).logException
  }

  def list(sortBy:String, order:SortOrder)
           (implicit offset:SearchOffset, limit:SearchSize):Future[SearchResponse] = {
    es.doSearch(search.in(indexName -> "view").from(offset.offset).size(limit.limit).sort(by field sortBy order order)).logException
  }

  def stream[U](sortBy:String, order:SortOrder, batchSize:SearchSize)(implicit reader: JsonReader[U]): Source[U, ActorRef] = {
    val retrieveBatch:(BatchPublisher.Offset, BatchPublisher.Size) ⇒ Future[Seq[U]] = { (limit:Int, size:Int) ⇒
      list(sortBy, order)(limit, size).mapResults[U](reader, log)
    }
    Source.actorPublisher[U](Props(new BatchPublisher[U](batchSize.limit)(retrieveBatch)))
  }

  def aggregate(aggDef:AbstractAggregationDefinition*)
               (implicit size:DocumentSize):Future[SearchResponse] = {
    es.doSearch(search.in(indexName -> "view").size(size.size).aggregations(aggDef)).logException
  }

  def queryFields(sortBy:String, order:SortOrder, fields:String*)
               (queryDef:QueryDefinition)
               (implicit offset:SearchOffset, limit:SearchSize):Future[SearchResponse] = {
    es.doSearch(search.in(indexName -> "view").from(offset.offset).size(limit.limit).fields(fields:_*).query(queryDef).sort(by field sortBy order order)).logException
  }

  def getFields(sortBy:String, order:SortOrder, fields:String*)
               (implicit offset:SearchOffset, limit:SearchSize):Future[SearchResponse] = {
    es.doSearch(search.in(indexName -> "view").from(offset.offset).size(limit.limit).fields(fields:_*).sort(by field sortBy order order)).logException
  }


  def createIndex():Future[CreateIndexResponse] = {
    val f = es.client.execute(create index indexName mappings("view" as indexDefinition))
    f.recover {
      case ex: Throwable => log.error(s"Could not create index '$indexName' -> 'view'", ex)
    }
    f
  }

  def clearIndex(): Future[DeleteIndexResponse] = {
    val f = es.client.execute(deleteIndex(indexName))
    f.map { case r =>
      if(!r.isAcknowledged)
        throw new Exception("Could not delete index")
      else
        r
    }
  }
}

trait ElasticSearchIndexDefinitionLike extends ElasticSearchTypes  {
  def indexDefinition:Seq[TypedFieldDefinition]
  def es:ElasticSearch
  def log:LoggingAdapter
  def indexName: IndexName

  def createIndex():Future[CreateIndexResponse]
  def clearIndex(): Future[DeleteIndexResponse]
}

trait ElasticSearchUtils extends JsonMarshalling
{
  implicit def ec:ExecutionContext

  protected implicit def genericToOption[U](u: U): Option[U] = Option(u)

  protected def multiRegex(key: String, criteria: String, splitOn: String = " ") = {
    val words = criteria.toLowerCase.replaceAll(splitOn + splitOn, splitOn).split(splitOn).toSeq
    words.map {
      word =>
        regexQuery(key, ".*" + word + ".*")
    }
  }

  protected implicit class SearchResponseOps(searchResult: SearchResponse)
  {
    def logException(implicit log: LoggingAdapter, ec: ExecutionContext): SearchResponse = {
      searchResult.getShardFailures.foreach { f =>
        log.error(s"Failure on ${f.index()}: ${f.reason()}")
      }
      searchResult
    }
  }

  protected implicit class FutureSearchResponseOps(futureSearchResult: Future[SearchResponse])
  {
    /**
     * Maps all json documents to case classes. If 1 fails, others may still succeed.
     * @tparam U    The type to convert to.
     * @return      `Future(Some[U])` if successful, `Future[None]` otherwise
     */
    def mapResults[U](implicit reader: JsonReader[U], log:LoggingAdapter):Future[Seq[U]] = {
      futureSearchResult.map {
        res =>
          res.getHits.getHits.toList.flatMap { hit =>
            val json = hit.getSourceAsString
            val t = Try(json.asObject[U])
            t.recover { case ex: Throwable =>
              log.error(s"{}: {} while parsing JSON:\n{}",ex.getClass.getName, ex.getMessage, json)
              if(log.isDebugEnabled)
                ex.printStackTrace()
            }
            t.toOption
          }
      }
    }
    /**
     * Maps the first json document to a case class.
     * @tparam U    The type to convert to
     * @return      `Future(Some[U])` if successful, `Future[None]` otherwise
     */
    def mapSingle[U](implicit reader: JsonReader[U], log:LoggingAdapter):Future[Option[U]] = {
      futureSearchResult.map { res =>
        res.getHits.getHits.toList.headOption.flatMap { json ⇒
          Try[U](json.getSourceAsString.asObject[U]).recover { case ex:Throwable =>
            log.error(s"{}: {} while parsing JSON:\n{}",ex.getClass.getName, ex.getMessage, json)
            if(log.isDebugEnabled)
              ex.printStackTrace()
            throw ex;
          }.toOption
        }
      }
    }
  }

  protected implicit class FutureOps[U](futureIndexResponse: Future[U])
  {
    def logException(implicit log: LoggingAdapter, ec: ExecutionContext): Future[U] = {
      futureIndexResponse.recover {
        case ex: Throwable =>
          val cause = Option(ex.getCause)
          log.error(s"FATAL FAILURE: Error in the index: ${ex.getMessage} ${cause.map(_.getMessage).getOrElse("")}")
          if(log.isDebugEnabled)
            ex.printStackTrace()
      }
      futureIndexResponse
    }
  }

  protected implicit class FutureIndexResponseOps(futureIndexResponse: Future[IndexResponse])
  {
    /**
     * @return Future(true) if the item was created in the index Future(false) otherwise
     */
    def mapCreated = futureIndexResponse.map(_.isCreated)
  }

  implicit class AggregationExtracter(res:SearchResponse) {
    def getAggregation[U <: Aggregation](aggregationName:String):U = {
      res.getAggregations.getAsMap.get(aggregationName).asInstanceOf[U]
    }
  }
}