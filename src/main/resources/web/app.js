/**
 * Created by terryhendrix on 09/07/16.
 */
var scanMap = angular.module("scanMap", ['asyncHttp', 'uiGmapgoogle-maps']);

var SCALING_FACTOR = 4;
var MAX_SCALE = 10;
var MIN_SIZE = 16;

scanMap.controller("SpyMapController", function($scope, asyncHttp, uiGmapGoogleMapApi) {
    $scope.ready = false;
    $scope.loaded = false;
    $scope.map = { center: { latitude: 45, longitude: 4 }, zoom: 2 };
    $scope.modalsOpened = [];
    $scope._reload = false;

    $scope.icon = function(violation) {
        var size = MIN_SIZE;

        if(violation.violations.length < MAX_SCALE) {
            size += violation.violations.length * SCALING_FACTOR;
        } else {
            size += MAX_SCALE * SCALING_FACTOR;
        }

        return {
            url:"img/spy.png",
            scaledSize: new google.maps.Size(size, size, 'px', 'px'),
            anchor: new google.maps.Point(size/2,size/2)
        }
    };

    $scope.loadData = function() {
        asyncHttp.get("/api/v1/spymap/violators").then(function(violators) {
            $scope.violators = violators.map(function(violator) {
                violator['options'] = {
                    icon: $scope.icon(violator)
                };

                violator['scans'] =  violator.violations.fold("", function(acc, violation) {
                    return acc + (violation.port + "/" + violation.protocol + " " + violation.description + " - " + violation.dateDetected + " \n")
                });
                return violator;
            });
            $scope.loaded = true;
        })
    };

    $scope.reloadMap = function() {
        var ret = $scope._reload;
        if($scope._reload) {
            $scope._reload = false;
        }
        return ret;
    };

    $scope.showViolations = function(violator) {
        console.log(violator)
    };

    $scope.identifier = function(violator)  {
        if(violator.ip)
            return violator.ip.replace('.', '-').replace('.', '-').replace('.', '-').replace('.', '-');
    };

    $scope.openModal = function(violator) {
        console.info("Opening "+violator.ip);
        violator.open = true;
        $scope._reload = true;
        $scope.$apply();
    };

    $scope.closeModal = function(violator) {
        console.info("Closing "+violator.ip);
        violator.open = false;
        $scope.$apply();
    };

    $scope.isModalOpen = function(violator) {
        return violator.open;
    };

    $scope.loadData();

    uiGmapGoogleMapApi.then(function(maps) {
        console.info("Maps ready");
        console.info(maps);
        $scope.ready = true
    });
});