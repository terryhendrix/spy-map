package nl.bluesoft.scanmap.suite

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FeatureSpec, GivenWhenThen}

trait TestSuite extends FeatureSpec with GivenWhenThen with BeforeAndAfterAll with BeforeAndAfterEach with ScalaFutures with Eventually
{
  implicit val system = ActorSystem("ScanMapTest")
  implicit val log = system.log
  implicit val mat = ActorMaterializer()
  implicit override val patienceConfig = PatienceConfig(scaled(Span(5, Seconds)), scaled(Span(100, Millis)))


  override def afterAll() = {
    system.shutdown()
    system.awaitTermination()
  }

}
