package nl.bluesoft.scanmap
import nl.bluesoft.scanmap.cache.ViolationIndex
import nl.bluesoft.scanmap.suite.TestSuite
import nl.bluesoft.scanmap.system.ScanMapAnalyzer

class AnalyzerTest extends TestSuite
{
  feature("Stream") {
    scenario("1") {
      val index = new ViolationIndex()
      val analyzer  = new ScanMapAnalyzer(index)
      analyzer.indexAndAnalyze.futureValue
      Thread sleep 1500
      analyzer.analyse.futureValue
    }
  }
}
