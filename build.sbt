import Build._
import com.typesafe.sbt.packager.docker._

lazy val dashIt = (project in file("."))
  .enablePlugins(JavaAppPackaging, sbtdocker.DockerPlugin)
  .configs(IntegrationTest)
  .settings(Defaults.itSettings:_*)
  .settings(
    organization := "nl.bluesoft",
    name := "spy-map",
    version := "0.3.0",
    scalaVersion := "2.11.7",
    credentials += Credentials(Path.userHome / ".ivy2" / ".bluesoft-credentials"),
    testOptions in ThisBuild += Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/test-reports"),
    libraryDependencies ++= Seq(
      // Run time dependencies
      "com.typesafe.akka"           %% "akka-actor"                           % V.akka,
      "com.typesafe.akka"           %% "akka-slf4j"                           % V.akka,
      "com.typesafe.akka"           %% "akka-cluster"                         % V.akka,
      "com.typesafe.akka"           %% "akka-camel"                           % V.akka,
      "com.typesafe.akka"           %% "akka-contrib"                         % V.akka,
      "com.typesafe.akka"           %% "akka-persistence"                     % V.akka,
      "com.typesafe.akka"           %% "akka-persistence-query-experimental"  % V.akka,
      "com.typesafe.akka"           %% "akka-stream"                          % V.akka,
      "com.typesafe.akka"           %% "akka-http-core"                       % V.akka,
      "com.typesafe.akka"           %% "akka-http-experimental"               % V.akka,
      "com.typesafe.akka"           %% "akka-http-spray-json-experimental"    % V.akka,
      "org.iq80.leveldb"             % "leveldb"                              % "0.7",
      "org.fusesource.leveldbjni"    % "leveldbjni-all"                       % "1.8",
      "org.codehaus.janino"          % "janino"                               % "2.6.1",
      "org.codehaus.groovy"          % "groovy-all"                           % "1.8.2",
      "org.scala-lang"               % "scala-library"                        % scalaVersion.value,
      "ch.qos.logback"               % "logback-classic"                      % "1.1.2",
      "com.github.dnvriend"         %% "akka-persistence-inmemory"            % "1.2.15",
      "com.sksamuel.elastic4s"      %% "elastic4s-core"                       % "1.6.5",
      // Testing dependencies
      "com.typesafe.akka"           %% "akka-testkit"                         % V.akka    % "it,test",
      "com.typesafe.akka"           %% "akka-stream-testkit"                  % V.akka    % "it,test",
      "com.typesafe.akka"           %% "akka-http-testkit"                    % V.akka    % "it,test",
      "org.scalatest"               %% "scalatest"                            % "2.1.4"   % "it,test",
      "org.pegdown"                  % "pegdown"                              % "1.4.2"   % "it,test"
    ),
    parallelExecution in IntegrationTest := false,
    autoCompilerPlugins := true,
    scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8"),
    publishMavenStyle := true,
    publishArtifact in test := false,
    fork := true,
    fork in run := true,
    fork in test := true,
    parallelExecution in test := false,
    dockerBaseImage := "java:8-jre",
    packageName in Docker := "dockerhub.bluesoftsolutions.nl:5000/bluesoft/"+name.value,
    dockerExposedPorts := Seq(8080, 9200),
    dockerExposedVolumes := Seq("/appl/log"),
    defaultLinuxInstallLocation in Docker := "/appl",
    dockerCommands += Cmd("USER", "root"),
    dockerCommands += Cmd("ENV", "DEV_MODE=false"),
    dockerCommands += Cmd("ENV", "TZ=Europe/Amsterdam"),
    dockerCommands += Cmd("RUN", """ echo "Europe/Amsterdam" > /etc/timezone """)
  )
