#!/bin/bash

run() {
    if [ $# -eq 0 ];
        then
            echo "No username supplied to run playbook"
        else
        if [ $# -eq 2 ];
            then
                echo "Dry run playbook ($PLAYBOOK) on inventory ($INVENTORY_FILE) with remote user ($1)."
                ansible-playbook --check --inventory-file=$INVENTORY_FILE --user=$1 $PLAYBOOK $2
            else
                echo "Running playbook ($PLAYBOOK) on inventory ($INVENTORY_FILE) with remote user ($1)."
                ansible-playbook --inventory-file=$INVENTORY_FILE --user=$1 $PLAYBOOK $2
        fi
    fi
}

PLAYBOOK=playbook.yml
INVENTORY_FILE=inventory.inv

run "$@"